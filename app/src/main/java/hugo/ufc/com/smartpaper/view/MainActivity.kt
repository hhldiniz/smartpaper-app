package hugo.ufc.com.smartpaper.view

import android.app.Activity
import android.app.AlertDialog
import android.content.Intent
import android.os.Bundle
import android.speech.RecognizerIntent
import android.view.Menu
import android.view.MenuItem
import android.view.View
import hugo.ufc.com.smartpaper.R
import hugo.ufc.com.smartpaper.presenter.MainActivityPresenter
import hugo.ufc.com.smartpaper.utils.RequestCodes
import hugo.ufc.com.smartpaper.view.login.LoginActivity
import hugo.ufc.com.smartpaper.view.searchResult.ResultActivity
import hugo.ufc.com.smartpaper.view.signup.SignupActivity
import kotlinx.android.synthetic.main.activity_main.*
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.launch
import org.json.JSONArray

class MainActivity : Activity() {
    private val presenter = MainActivityPresenter()

    private fun search() {
        GlobalScope.launch {
            val result: JSONArray
            runOnUiThread {
                progressBar.visibility = View.VISIBLE
            }
            try
            {
                result = presenter.search(search_key_input.text.toString())
                if(result.get(0) != -1)
                {
                    val intent = Intent(this@MainActivity, ResultActivity::class.java)
                    intent.putExtra("result", result.toString())
                    startActivity(intent)
                }
            }catch (e: Exception)
            {
                e.printStackTrace()
                runOnUiThread {
                    AlertDialog.Builder(this@MainActivity)
                        .setMessage(R.string.retrieve_data_error_msg)
                        .setTitle(R.string.retrieve_data_error_title)
                        .setNeutralButton(R.string.ok, null)
                        .create().show()
                }
            }
        }.invokeOnCompletion {
            runOnUiThread {
                progressBar.visibility = View.INVISIBLE
            }
        }
    }

    private fun voiceSearch() {
        presenter.speechToText(this)
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        search_btn.setOnClickListener { search() }
        fab.setOnClickListener { voiceSearch() }
        setActionBar(findViewById(R.id.toolbar))
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        when(requestCode)
        {
            RequestCodes.TEXT_TO_SPEECH.code->
            {
                if(resultCode == RESULT_OK && data != null)
                {
                    val result = data.getStringArrayListExtra(RecognizerIntent.EXTRA_RESULTS)
                    search_key_input.setText(result[0].toString())
                    search()
                }
            }
        }
    }

    override fun onOptionsItemSelected(item: MenuItem?): Boolean {
        return when(item?.itemId) {
            R.id.login_action->{
                startActivity(Intent(this@MainActivity, LoginActivity::class.java))
                true
            }
            R.id.signup_action->{
                startActivity(Intent(this@MainActivity, SignupActivity::class.java))
                true
            }
            else->{
                super.onOptionsItemSelected(item)
            }
        }
    }

    override fun onCreateOptionsMenu(menu: Menu?): Boolean {
        menuInflater.inflate(R.menu.main_activity_actions_menu, menu)
        return super.onCreateOptionsMenu(menu)
    }

}
