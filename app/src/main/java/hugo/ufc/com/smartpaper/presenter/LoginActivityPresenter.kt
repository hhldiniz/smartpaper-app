package hugo.ufc.com.smartpaper.presenter

import hugo.ufc.com.smartpaper.presenter.interfaces.LoginActivityPresenterInterface
import hugo.ufc.com.smartpaper.utils.LoggedUser
import hugo.ufc.com.smartpaper.utils.NetworkRequester
import org.json.JSONObject

class LoginActivityPresenter: LoginActivityPresenterInterface {
    override fun updateLoggedUserInfo(userInfo: JSONObject) {
        LoggedUser.name = userInfo.getString("name")
        LoggedUser.email = userInfo.getString("email")
        LoggedUser.username = userInfo.getString("username")
    }

    override fun login(username: String, password: String): Boolean {
        val networkRequester = NetworkRequester()
        val params = HashMap<String, Any>()
        params["username"] = username
        params["password"] = password
        networkRequester.httpMethod = "POST"
        networkRequester.endpoint = "authenticate"
        networkRequester.params = params
        val result = networkRequester.request()
        updateLoggedUserInfo(result.getJSONObject(0).getJSONObject("user"))
        return result.getJSONObject(0).getBoolean("result")
    }
}