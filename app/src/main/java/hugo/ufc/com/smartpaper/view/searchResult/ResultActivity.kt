package hugo.ufc.com.smartpaper.view.searchResult

import android.app.Activity
import android.os.Bundle
import android.widget.ArrayAdapter
import hugo.ufc.com.smartpaper.R
import hugo.ufc.com.smartpaper.presenter.ResultActivityPresenter
import kotlinx.android.synthetic.main.activity_result.*
import org.json.JSONArray

class ResultActivity : Activity() {
    private val presenter = ResultActivityPresenter()

    private lateinit var resultListAdapter: ArrayAdapter<String>
    private fun populateResultList() {
        val resultObject: JSONArray
        val bundle = intent.extras
        resultObject = if(bundle != null && bundle["result"] != null) {
            JSONArray(bundle["result"]?.toString())
        } else
            JSONArray()
        presenter.populateResultList(resultObject, resultListAdapter)
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_result)
        resultListAdapter = ArrayAdapter(this, android.R.layout.simple_list_item_1)
        populateResultList()
        result_list.adapter = resultListAdapter
    }
}
