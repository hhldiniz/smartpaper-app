package hugo.ufc.com.smartpaper.view.searchResult

import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.ViewGroup
import hugo.ufc.com.smartpaper.R
import hugo.ufc.com.smartpaper.model.Article

class ResultViewAdapter: RecyclerView.Adapter<ResultViewHolder>() {
    private val resultList = ArrayList<Article>()

    override fun onCreateViewHolder(p0: ViewGroup, p1: Int): ResultViewHolder {
        val view = LayoutInflater.from(p0.context)
            .inflate(R.layout.result_item_list_layout, p0, false)
        return ResultViewHolder(view)
    }

    override fun getItemCount(): Int {
        return resultList.size
    }

    override fun onBindViewHolder(p0: ResultViewHolder, p1: Int) {
        p0.bindData(resultList[p1])
    }

    fun addItem(item: Article)
    {
        resultList.add(item)
        notifyDataSetChanged()
    }

    fun removeItem(item: Article)
    {
        resultList.remove(item)
        notifyDataSetChanged()
    }
}