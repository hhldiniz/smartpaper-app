package hugo.ufc.com.smartpaper.presenter

import android.app.Activity
import android.content.Intent
import android.graphics.Bitmap
import android.graphics.drawable.BitmapDrawable
import android.widget.ImageButton
import hugo.ufc.com.smartpaper.R
import hugo.ufc.com.smartpaper.model.User
import hugo.ufc.com.smartpaper.presenter.interfaces.SignupActivityPresenterInterface
import hugo.ufc.com.smartpaper.utils.NetworkRequester
import hugo.ufc.com.smartpaper.utils.RequestCodes
import java.io.ByteArrayOutputStream

class SignupActivityPresenter(private val activity:Activity): SignupActivityPresenterInterface {
    override fun pickPhoto() {
        val intent = Intent()
        intent.type = "image/*"
        intent.action = Intent.ACTION_GET_CONTENT
        activity.startActivityForResult(Intent.createChooser(intent,
            activity.getString(R.string.photo_picker_chooser_title)), RequestCodes.PICK_PHOTO.code)
    }

    override fun validEmail(email: String): String? {
        val regex = Regex("^([a-zA-Z0-9_\\-.]+)@([a-zA-Z0-9_\\-.]+)\\.([a-zA-Z]{2,5})\$")
        if(!regex.matches(email))
            return activity.getString(R.string.email_invalid)
        return null
    }

    override fun validateName(name: String): String? {
        if(name.isEmpty())
            return activity.getString(R.string.required_field)
        return null
    }

    override fun validateUsername(username: String): String? {
        if(username.isEmpty())
            return activity.getString(R.string.required_field)
        return null
    }

    override fun validatePassword(password: String): String? {
        if(password.isEmpty())
            return activity.getString(R.string.required_field)
        return null
    }

    override fun validatePasswordConfirmation(password: String, passwordConfirmation: String): String? {
        if(passwordConfirmation.isEmpty())
            return activity.getString(R.string.required_field)
        if(password != passwordConfirmation)
            return activity.getString(R.string.password_confirmation_invalid)
        return null
    }

    override fun signup(user: User): Boolean {
        val networkRequester = NetworkRequester()
        networkRequester.httpMethod = "POST"
        networkRequester.endpoint = "signup"
        val params = HashMap<String, Any>()
        params["name"] = user.name
        params["email"] = user.email
        params["password"] = user.password
        params["username"] = user.username
        params["photo"] = getPhotoByteArray()
        networkRequester.params = params
        return networkRequester.request().getJSONObject(0).getBoolean("result")
    }

    private fun getPhotoByteArray(): ByteArray
    {
        val drawable = activity.findViewById<ImageButton>(R.id.photo_input).drawable
        val bitmap = (drawable as BitmapDrawable).bitmap
        val stream = ByteArrayOutputStream()
        bitmap.compress(Bitmap.CompressFormat.JPEG, 100, stream)
        return stream.toByteArray()
    }
}