package hugo.ufc.com.smartpaper.view.signup

import android.app.Activity
import android.app.AlertDialog
import android.content.Intent
import android.graphics.drawable.Drawable
import android.os.Bundle
import android.os.Looper
import android.text.Editable
import android.text.TextWatcher
import hugo.ufc.com.smartpaper.R
import hugo.ufc.com.smartpaper.model.User
import hugo.ufc.com.smartpaper.presenter.SignupActivityPresenter
import hugo.ufc.com.smartpaper.utils.RequestCodes
import kotlinx.android.synthetic.main.signup_activity_layout.*
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.launch

class SignupActivity: Activity() {
    private lateinit var signupActivityPresenter : SignupActivityPresenter
    private lateinit var user: User
    private var formIsValid = true
    private fun signup(user: User)
    {
        var signupResult = false
        GlobalScope.launch {
            try {
                signupResult = signupActivityPresenter.signup(user)
            }catch (e: Exception)
            {
                e.printStackTrace()
            }
        }.invokeOnCompletion {
            runOnUiThread {
                if(signupResult)
                {
                    if(Looper.myLooper() == null)
                        Looper.prepare()
                    AlertDialog.Builder(this).setTitle(R.string.generic_success_title)
                        .setMessage(R.string.signup_success_msg).setNeutralButton(R.string.ok){_, _ -> finish() }.create().show()
                }
                else
                    AlertDialog.Builder(this).setTitle(R.string.generic_error_title)
                        .setMessage(R.string.generic_error_msg).setNeutralButton(R.string.ok, null).create().show()
            }
        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        signupActivityPresenter = SignupActivityPresenter(this)
        setContentView(R.layout.signup_activity_layout)
        end_signup_btn.setOnClickListener {
            if(formIsValid)
            {
                if(!::user.isInitialized)
                    user = User(username_input.text.toString())
                else
                    user.username = username_input.text.toString()
                user.name = name_input.text.toString()
                user.password = password_input.text.toString()
                user.email = email_input.text.toString()
                signup(user)
            }
            else
                AlertDialog.Builder(this@SignupActivity).setTitle(R.string.generic_error_title)
                    .setMessage(R.string.form_invalid_msg).setNeutralButton(R.string.ok, null).create().show()
        }
        name_input.addTextChangedListener(object: TextWatcher{
            override fun afterTextChanged(p0: Editable?) {

            }

            override fun beforeTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {

            }

            override fun onTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {
                name_input.error = signupActivityPresenter.validateName(p0.toString())
                formIsValid = name_input.error == null
            }
        })
        username_input.addTextChangedListener(object : TextWatcher{
            override fun afterTextChanged(p0: Editable?) {

            }

            override fun beforeTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {

            }

            override fun onTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {
                username_input.error = signupActivityPresenter.validateUsername(p0.toString())
                formIsValid = username_input.error == null
            }
        })
        email_input.addTextChangedListener(object : TextWatcher{
            override fun afterTextChanged(p0: Editable?) {

            }

            override fun beforeTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {

            }

            override fun onTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {
                email_input.error = signupActivityPresenter.validEmail(p0.toString())
                formIsValid = email_input.error == null
            }
        })
        password_input.addTextChangedListener(object : TextWatcher{
            override fun afterTextChanged(p0: Editable?) {

            }

            override fun beforeTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {

            }

            override fun onTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {
                password_input.error = signupActivityPresenter.validatePassword(p0.toString())
                formIsValid = password_input.error == null
            }
        })
        confirm_password_input.addTextChangedListener(object : TextWatcher{
            override fun afterTextChanged(p0: Editable?) {

            }

            override fun beforeTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {

            }

            override fun onTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {
                confirm_password_input.error = signupActivityPresenter.validatePasswordConfirmation(
                    password_input.text.toString(), p0.toString())
                formIsValid = confirm_password_input.error == null
            }
        })
        photo_input.setOnClickListener { signupActivityPresenter.pickPhoto() }
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        when(requestCode)
        {
            RequestCodes.PICK_PHOTO.code->{
                if(resultCode == RESULT_OK && data != null && data.data != null)
                {
                    val inputStream = contentResolver.openInputStream(data.data!!)
                    val drawable = Drawable.createFromStream(inputStream, "userphoto")
                    photo_input.setImageDrawable(drawable)
                }
            }
        }
    }
}