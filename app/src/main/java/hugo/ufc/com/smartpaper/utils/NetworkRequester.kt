package hugo.ufc.com.smartpaper.utils

import android.util.Log
import org.json.JSONArray
import java.io.*
import java.net.HttpURLConnection
import java.net.URL

class NetworkRequester{
    lateinit var httpMethod: String
    lateinit var params: HashMap<String, Any>
    lateinit var endpoint: String
    lateinit var headers : HashMap<String, String>
//    private val host = "https://smartpaperserver.herokuapp.com/"
    private val host = "http://192.168.0.11:8000/"

    fun request(): JSONArray
    {
        val url = URL(host+endpoint)
        val urlConnection = url.openConnection() as HttpURLConnection
        urlConnection.requestMethod = httpMethod
        urlConnection.connectTimeout = 10000
        if(::headers.isInitialized)
            setRequestHeaders(urlConnection)
        val out = BufferedOutputStream(urlConnection.outputStream)
        val writer = BufferedWriter(OutputStreamWriter(out, "UTF-8"))
        writer.write(getPostData())
        writer.flush()
        writer.close()
        out.close()
        when(urlConnection.responseCode){
            HttpURLConnection.HTTP_OK->{
                val response = StringBuilder()
                val bufferedReader = BufferedReader(InputStreamReader(urlConnection.inputStream))
                bufferedReader.forEachLine {response.append(it)}
                return JSONArray(response.toString())

            }
            else->{
                println(getPostData())
                println("$host$endpoint ${urlConnection.responseCode}")
                throw Exception()
            }
        }
    }

    private fun getPostData(): String
    {
        val sb = StringBuilder()
        var firstParam = true
        params.entries.forEach {
            if(!firstParam)
                sb.append("&")
            firstParam = false
            sb.append("${it.key}=${it.value}")
        }
        Log.d("postData", sb.toString())
        return sb.toString()
    }

    private fun setRequestHeaders(httpURLConnection: HttpURLConnection)
    {
        headers.entries.forEach {
            httpURLConnection.setRequestProperty(it.key, it.value)
        }
    }
}