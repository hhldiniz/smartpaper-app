package hugo.ufc.com.smartpaper.presenter

import android.widget.ArrayAdapter
import hugo.ufc.com.smartpaper.presenter.interfaces.ResultActivityPresenterInterface
import org.json.JSONArray
import org.json.JSONObject

class ResultActivityPresenter: ResultActivityPresenterInterface {

    override fun populateResultList(resultObject: JSONArray, adapter: ArrayAdapter<String>)
    {
        for(i in 0 until resultObject.length())
        {
            for(j in 0 until (resultObject[i] as JSONArray).length())
            {
                val result = JSONObject((resultObject[i] as JSONArray)[j].toString())
                result.getString("name").split(",").forEach { adapter.add(it.replace(Regex("[\\[\\],\"]"), "")) }
            }
        }
    }
}