package hugo.ufc.com.smartpaper.model

data class User(var username: String)
{
    var password = ""
    var email = ""
    var name = ""
}