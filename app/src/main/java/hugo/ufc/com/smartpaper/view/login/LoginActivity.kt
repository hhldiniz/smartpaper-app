package hugo.ufc.com.smartpaper.view.login

import android.app.Activity
import android.app.AlertDialog
import android.os.Bundle
import android.os.Looper
import hugo.ufc.com.smartpaper.R
import hugo.ufc.com.smartpaper.presenter.LoginActivityPresenter
import hugo.ufc.com.smartpaper.utils.exceptions.LoginException
import kotlinx.android.synthetic.main.login_activity_layout.*
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.launch

class LoginActivity: Activity() {
    private val presenter = LoginActivityPresenter()
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.login_activity_layout)
        login_btn.setOnClickListener {
            GlobalScope.launch {
                if(Looper.myLooper() == null)
                    Looper.prepare()
                try {
                    if(presenter.login(username_input.text.toString(), password_input.text.toString()))
                        finish()
                    else
                        throw LoginException(getString(R.string.login_error))
                }catch (e: LoginException)
                {
                    runOnUiThread {
                        AlertDialog.Builder(this@LoginActivity)
                            .setTitle(R.string.generic_error_title)
                            .setMessage(e.message)
                            .setNeutralButton(R.string.ok) { _, _ ->  }.create().show()
                    }
                }catch (e: Exception)
                {
                    runOnUiThread {
                        showGenericError()
                    }
                    e.printStackTrace()
                }
            }.invokeOnCompletion {throwable->
                if(throwable != null)
                {
                    showGenericError()
                    throwable.printStackTrace()
                }
            }
        }
    }

    private fun showGenericError()
    {
        AlertDialog.Builder(this)
            .setTitle(R.string.generic_error_title)
            .setMessage(R.string.generic_error_msg)
            .setNeutralButton(R.string.ok, null).create().show()
    }
}