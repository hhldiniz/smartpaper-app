package hugo.ufc.com.smartpaper.view.searchResult

import android.support.v7.widget.RecyclerView
import android.view.View
import hugo.ufc.com.smartpaper.model.Article
import kotlinx.android.synthetic.main.result_item_list_layout.view.*

class ResultViewHolder(itemView: View): RecyclerView.ViewHolder(itemView)
{
    fun bindData(article: Article)
    {
        itemView.article_title.text = article.getTitle()
        itemView.article_abstract.text = article.getAbstract()
    }
}