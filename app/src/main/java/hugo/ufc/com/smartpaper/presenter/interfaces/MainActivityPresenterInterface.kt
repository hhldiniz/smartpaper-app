package hugo.ufc.com.smartpaper.presenter.interfaces

import android.app.Activity
import org.json.JSONArray

interface MainActivityPresenterInterface {
    fun search(key: String): JSONArray
    fun speechToText(activity: Activity)
}