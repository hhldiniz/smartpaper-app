package hugo.ufc.com.smartpaper.model

data class Article(private val id: Int) {
    private lateinit var title: String
    private lateinit var content: String
    private lateinit var abstract: String

    fun setTitle(title: String)
    {
        this.title = title
    }

    fun setContent(content: String)
    {
        this.content = content
    }

    fun setAbstract(abstract: String)
    {
        this.abstract = abstract
    }

    fun getTitle(): String
    {
        return title
    }

    fun getContent(): String
    {
        return content
    }

    fun getAbstract(): String
    {
        return abstract
    }
}