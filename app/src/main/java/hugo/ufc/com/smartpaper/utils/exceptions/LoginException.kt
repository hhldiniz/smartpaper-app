package hugo.ufc.com.smartpaper.utils.exceptions

class LoginException(message: String): Exception(message)