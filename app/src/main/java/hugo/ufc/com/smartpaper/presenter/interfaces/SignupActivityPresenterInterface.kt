package hugo.ufc.com.smartpaper.presenter.interfaces

import hugo.ufc.com.smartpaper.model.User

interface SignupActivityPresenterInterface {
    fun signup(user: User): Boolean
    fun validateName(name: String): String?
    fun validateUsername(username: String): String?
    fun validatePassword(password: String): String?
    fun validEmail(email: String): String?
    fun validatePasswordConfirmation(password: String, passwordConfirmation: String): String?
    fun pickPhoto()
}