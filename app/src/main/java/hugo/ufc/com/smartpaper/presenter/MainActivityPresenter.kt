package hugo.ufc.com.smartpaper.presenter

import android.app.Activity
import android.app.AlertDialog
import android.content.ActivityNotFoundException
import android.content.Intent
import android.speech.RecognizerIntent
import hugo.ufc.com.smartpaper.R
import hugo.ufc.com.smartpaper.presenter.interfaces.MainActivityPresenterInterface
import hugo.ufc.com.smartpaper.utils.NetworkRequester
import hugo.ufc.com.smartpaper.utils.RequestCodes
import org.json.JSONArray
import java.util.*

class MainActivityPresenter: MainActivityPresenterInterface {

    override fun search(key: String): JSONArray
    {
        val responseData = JSONArray()
        responseData.put(0, -1)
        val networkRequester = NetworkRequester()
        val hashMap = HashMap<String, Any>()
        hashMap["key"] = key
        hashMap["hidden"] = "search"
        networkRequester.endpoint = ""
        networkRequester.httpMethod = "POST"
        networkRequester.params = hashMap
        return networkRequester.request()
    }

    override fun speechToText(activity: Activity)
    {
        val intent = Intent(RecognizerIntent.ACTION_RECOGNIZE_SPEECH)
        intent.putExtra(RecognizerIntent.EXTRA_LANGUAGE_MODEL, RecognizerIntent.LANGUAGE_MODEL_FREE_FORM)
        intent.putExtra(RecognizerIntent.EXTRA_LANGUAGE, Locale.getDefault())
        intent.putExtra(RecognizerIntent.EXTRA_PROMPT, activity.getString(R.string.speech_dialog_msg))
        try {
            activity.startActivityForResult(intent, RequestCodes.TEXT_TO_SPEECH.code)
        }catch (e: ActivityNotFoundException)
        {
            AlertDialog.Builder(activity)
                .setMessage(activity.getString(R.string.device_not_supported))
                .setTitle(activity.getString(R.string.retrieve_data_error_title))
                .setNeutralButton(activity.getString(R.string.ok), null)
                .create().show()
        }
    }
}