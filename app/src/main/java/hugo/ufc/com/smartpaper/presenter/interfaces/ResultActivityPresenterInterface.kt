package hugo.ufc.com.smartpaper.presenter.interfaces

import android.widget.ArrayAdapter
import org.json.JSONArray

interface ResultActivityPresenterInterface {
    fun populateResultList(resultObject: JSONArray, adapter: ArrayAdapter<String>)
}