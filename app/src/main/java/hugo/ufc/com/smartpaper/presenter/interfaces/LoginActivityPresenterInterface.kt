package hugo.ufc.com.smartpaper.presenter.interfaces

import org.json.JSONObject

interface LoginActivityPresenterInterface {
    fun login(username: String, password: String): Boolean
    fun updateLoggedUserInfo(userInfo: JSONObject)
}