package hugo.ufc.com.smartpaper.utils

enum class RequestCodes(val code: Int) {
    TEXT_TO_SPEECH(200),
    PICK_PHOTO(100),
}